import { throttle } from './t'

export class C {
  i = 0

  @throttle(100, { start: true })
  inc() {
    this.i++
  }
}

describe('something', () => {
  beforeEach(() => {
    jest.useFakeTimers()
  })

  afterEach(() => {
    jest.clearAllTimers()
  })

  it('me1', () => {
    const a = new C()
    a.inc()
    a.inc()
    jest.runTimersToTime(100)
    a.inc()
    a.inc()
    expect(a.i).toEqual(2)
  })

  it('me2', () => {
    const a = new C()
    a.inc()
    a.inc()
    jest.runTimersToTime(100)
    a.inc()
    a.inc()
    expect(a.i).toEqual(2)
  })
})

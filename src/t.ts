interface ThrottleOpts {
  start: boolean
  middle: boolean
  once: boolean
}

interface ThrottleState {
  start: boolean
  last: number
  timer: number | undefined
  cancelled: boolean
}

export function throttle(wait = 0, options: Partial<ThrottleOpts> = {}) {
  let stateMap = new WeakMap<any, ThrottleState>()

  const opts: ThrottleOpts = Object.assign(
    { start: true, middle: true, once: false },
    options,
  )

  return (proto: any, name: any, descriptor: any) => {
    if (typeof descriptor?.value !== 'function')
      throw new Error('throttle can only decorate methods')

    const stateKey = `__throttle_opts_${name}`
    const fn = descriptor.value

    descriptor.value = function <T extends unknown[]>(
      this: any,
      ...args: T
    ): unknown {
      const state = this[stateKey]
      if (state.cancelled) return
      const delta = Date.now() - state.last
      state.last = Date.now()
      if (state.start) {
        state.start = false
        fn.apply(this, args)
        if (opts.once) {
          window.clearTimeout(state.timer)
          state.cancelled = true
        }
      } else if ((opts.middle && delta < wait) || !opts.middle) {
        window.clearTimeout(state.timer)
        state.timer = window.setTimeout(
          () => {
            state.last = Date.now()
            fn.apply(this, args)
            if (opts.once) {
              window.clearTimeout(state.timer)
              state.cancelled = true
            }
          },
          !opts.middle ? wait : wait - delta,
        )
      }
    }

    Object.defineProperty(proto, name, descriptor)
    Object.defineProperty(proto, stateKey, {
      configurable: true,
      get() {
        let state = stateMap.get(this)
        if (!state) {
          state = {
            start: opts.start,
            last: 0,
            timer: undefined,
            cancelled: false,
          }
          stateMap.set(this, state)
        }
        return state
      },
    })
  }
}

export interface ThrottleOptions {
  /**
   * fire immediately on the first call.
   */
  start?: boolean
  /**
   * fire as soon as `wait` has passed.
   */
  middle?: boolean
  /**
   * cancel after the first successful call.
   */
  once?: boolean
}

export interface Throttled<T extends unknown[]> {
  (...args: T): void
  cancel(): void
}

interface ThrottleState {
  start: boolean
  last: number
  timer: number | undefined
  cancelled: boolean
}

const cache = new WeakMap<any, ThrottleState>()

function getState(arg: any, start: boolean): ThrottleState {
  const s = cache.get(arg)
  if (s) return s
  const ret: ThrottleState = {
    start,
    last: 0,
    timer: undefined,
    cancelled: false,
  }
  cache.set(arg, ret)
  return ret
}

export function throttle<T extends unknown[]>(
  callback: (...args: T) => unknown,
  wait = 0,
  { start = true, middle = true, once = false }: ThrottleOptions = {},
): Throttled<T> {
  let s!: ThrottleState

  function fn(this: unknown, ...args: T) {
    s = getState(this, start)
    if (s.cancelled) return
    const delta = Date.now() - s.last
    s.last = Date.now()
    if (s.start) {
      s.start = false
      callback.apply(this, args)
      if (once) fn.cancel()
    } else if ((middle && delta < wait) || !middle) {
      window.clearTimeout(s.timer)
      s.timer = window.setTimeout(
        () => {
          s.last = Date.now()
          callback.apply(this, args)
          if (once) fn.cancel()
        },
        !middle ? wait : wait - delta,
      )
    }
  }

  fn.cancel = function () {
    // broken if different fn called?
    window.clearTimeout(s.timer)
    s.cancelled = true
  }

  return fn
}

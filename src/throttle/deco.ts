import type { ThrottleOptions } from './fn'
import { throttle as $throttle } from './fn'

export function throttle<T>(
  target: T,
  propertyKey: string,
  descriptor: PropertyDescriptor,
): void

export function throttle(wait?: number, opts?: ThrottleOptions): MethodDecorator

export function throttle<
  Arg1 extends unknown | number,
  Arg2 extends string | ThrottleOptions
>(arg1: Arg1, arg2: Arg2, arg3?: PropertyDescriptor) {
  const deco = (
    proto: unknown,
    name: string,
    descriptor: PropertyDescriptor,
  ) => {
    if (typeof descriptor?.value !== 'function')
      throw new Error('throttle can only decorate methods')

    const fn = descriptor.value
    descriptor.value = $throttle(fn, arg1 as number, arg2 as ThrottleOptions)
    Object.defineProperty(proto, name, descriptor)
  }

  if (typeof arg1 === 'number')
    return deco as Arg1 extends number ? MethodDecorator : void

  deco(arg1, arg2 as string, arg3 as PropertyDescriptor)
}

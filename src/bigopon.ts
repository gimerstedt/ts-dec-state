import { throttle as $throttle } from '@github/mini-throttle'

let statePropSeed = 1

export function throttle(wait = 0, opts = {}) {
  // ensure that opt cant be accidentally mutated from outside
  opts = { ...opts }
  let stateMap = new WeakMap()

  return (proto: any, name: any, descriptor: any) => {
    if (!descriptor || typeof descriptor.value !== 'function') {
      throw new Error('debounce can only decorate functions')
    }

    const fn = descriptor.value
    let throttledFn = $throttle(fn, wait, opts)
    descriptor.value = throttledFn
    Object.defineProperty(proto, name, descriptor)
    Object.defineProperty(proto, `throttleOpts${statePropSeed++}`, {
      configurable: true,
      get() {
        let throttleState = stateMap.get(this)
        if (!throttleState) {
          // by default, set throttleState to provided options
          stateMap.set(this, (throttleState = opts))
        }
        return throttleState
      },
      set(v) {
        // todo: more normalization here
        stateMap.set(this, v)
        // API from he package
        throttledFn.cancel()
        throttledFn = $throttle(fn, wait, opts)
        // update the prototype, (not production recommended😬)
        Object.defineProperty(proto, name, {
          ...descriptor,
          value: throttledFn,
        })
      },
    })
  }
}
